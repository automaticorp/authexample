from flask import Flask, request
import sqlite3
import hashlib

app = Flask(__name__)


@app.route('/login', methods=["POST"])
def checkpasswords():
    conn = sqlite3.connect("login.db")
    c = conn.cursor()
    c.execute("""CREATE TABLE IF NOT EXISTS login(username,password)""")

    hasheduser = hashlib.sha256((request.form['username']).encode('utf-8')).hexdigest()
    hashedpass = hashlib.sha256((request.form['password']).encode('utf-8')).hexdigest()

    query = c.execute("SELECT * FROM login WHERE username == ? AND password == ?",
        (hasheduser, hashedpass))
    fetch = query.fetchall()

    if fetch:
        return "valid"
    else:
        return "No username/login combination found"


if __name__ == "__main__":
    app.run()
