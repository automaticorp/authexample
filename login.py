import requests
import sqlite3
from uuid import uuid4
import hashlib

def login(username, password):

    # username = username + uuid4().hex
    # password = password + uuid4().hex
    # I realized idk how to add a salt properly

    payload = {"username":username, "password":password}
    r = requests.post("http://127.0.0.1:5000/login", data=payload)

    return r.text

def addUser(user, password):

    # Hashes username and password
    hasheduser = hashlib.sha256(user.encode('utf-8')).hexdigest()
    hashedpassword = hashlib.sha256(password.encode('utf-8')).hexdigest()

    conn = sqlite3.connect('login.db')
    c = conn.cursor()

    # Checks if already in database (I really need to fix this part)
    query = c.execute("SELECT * FROM login WHERE username= ? AND password = ?",
                (hasheduser, hashedpassword))
    fetch = query.fetchall()
    if fetch:
        return("User/Pass combo already in database")

    c.execute("""INSERT INTO login(username, password) VALUES(?, ?)""",
        (hasheduser, hashedpassword))

    conn.commit()

    c.close()
    conn.close()

    return "Added"

# DELETE CURRENTLY DOESN'T WORK
def deleteUser(user, password):

    hasheduser = hashlib.sha256(user.encode('utf-8')).hexdigest()
    hashedpassword = hashlib.sha256(password.encode('utf-8')).hexdigest()

    conn = sqlite3.connect('login.db')
    c = conn.cursor()

    c.execute("""DELETE FROM login WHERE username= ? AND password= ?""",
        (hasheduser, hashedpassword))

    return "Deleted"
